<?php
/**
 * @file
 * banner_slider.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function banner_slider_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
  );
  // Exported menu: menu-banner-slider-management.
  $menus['menu-banner-slider-management'] = array(
    'menu_name' => 'menu-banner-slider-management',
    'title' => 'Banner Slider',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Banner Slider');
  t('Management');
  t('The <em>Management</em> menu contains links for administrative tasks.');


  return $menus;
}
