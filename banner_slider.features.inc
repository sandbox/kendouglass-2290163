<?php
/**
 * @file
 * banner_slider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function banner_slider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function banner_slider_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function banner_slider_image_default_styles() {
  $styles = array();

  // Exported image style: banner_slide.
  $styles['banner_slide'] = array(
    'name' => 'banner_slide',
    'label' => 'banner_slide',
    'effects' => array(
      11 => array(
        'label' => 'Manual Crop: Crop and scale',
        'help' => 'Crop and scale a user-selected area, respecting the ratio of the destination width and height.',
        'effect callback' => 'manualcrop_crop_and_scale_effect',
        'form callback' => 'manualcrop_crop_and_scale_form',
        'summary theme' => 'manualcrop_crop_and_scale_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 940,
          'height' => 256,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'banner_slide',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: banner_slide_thumb.
  $styles['banner_slide_thumb'] = array(
    'name' => 'banner_slide_thumb',
    'label' => 'banner_slide_thumb',
    'effects' => array(
      12 => array(
        'label' => 'Manual Crop: Reuse cropped style',
        'help' => 'Reuse a crop selection from another Manual Crop enabled image style.',
        'effect callback' => 'manualcrop_reuse_effect',
        'form callback' => 'manualcrop_reuse_form',
        'summary theme' => 'manualcrop_reuse_summary',
        'module' => 'manualcrop',
        'name' => 'manualcrop_reuse',
        'data' => array(
          'reusestyle' => 'banner_slide',
          'style_name' => 'banner_slide_thumb',
        ),
        'weight' => 0,
      ),
      13 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function banner_slider_node_info() {
  $items = array(
    'banner_slider' => array(
      'name' => t('Banner slideshow frame'),
      'base' => 'node_content',
      'description' => t('A slide for the banner slider which appears on the home page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('<b>Create a new slide for the banner slider which appears on the home page.</b>'),
    ),
  );
  return $items;
}
