<?php
/**
 * @file
 * banner_slider.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function banner_slider_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/structure/menu/manage/menu-banner-slider-management
  $menu_links['management:admin/structure/menu/manage/menu-banner-slider-management'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/structure/menu/manage/menu-banner-slider-management',
    'router_path' => 'admin/structure/menu/manage/%',
    'link_title' => 'Banner Slider',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'admin/structure/menu',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Banner Slider');


  return $menu_links;
}
