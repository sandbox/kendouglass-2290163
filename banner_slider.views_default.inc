<?php
/**
 * @file
 * banner_slider.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function banner_slider_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'banner_slider';
  $view->description = 'Public and administrative slideshow views. Utilizes the Flexslider jQuery plugin';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Banner Slider';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Banner Slider';
  $handler->display->display_options['css_class'] = 'banner-slider';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'banner_slider';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Draggableviews: Content */
  $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
  $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['label'] = '';
  $handler->display->display_options['fields']['draggableviews']['exclude'] = TRUE;
  $handler->display->display_options['fields']['draggableviews']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['save_button_label'] = 'Save sorting order';
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_type'] = 'div';
  $handler->display->display_options['fields']['edit_node']['element_class'] = 'edit-link';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['text'] = 'Edit this slide';
  /* Field: Content: Banner slider image */
  $handler->display->display_options['fields']['field_banner_slider_img']['id'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['table'] = 'field_data_field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['field'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slider_img']['alter']['path'] = '[field_banner_slider_url]';
  $handler->display->display_options['fields']['field_banner_slider_img']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['empty'] = 'No image!';
  $handler->display->display_options['fields']['field_banner_slider_img']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_banner_slider_img']['settings'] = array(
    'image_style' => 'banner_slide_thumb',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '30';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_type'] = 'strong';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['text'] = '<div class="banner-slide-text">
<h2 class="banner-slide-title">[title]</h2>
<div class="banner-slide-caption">[body]</div>
<!--<div class="banner-slide-attrib">[field_banner_slide_credit]</div>-->
</div>';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '100';
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['preserve_tags'] = '<a> <b> <strong>';
  $handler->display->display_options['fields']['body']['element_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['empty'] = 'No caption!';
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '100',
  );
  /* Field: Content: Image Source/Attribution */
  $handler->display->display_options['fields']['field_banner_slide_credit']['id'] = 'field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['table'] = 'field_data_field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['field'] = 'field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slide_credit']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['type'] = 'text_plain';
  /* Field: Content: Target path or URL */
  $handler->display->display_options['fields']['field_banner_slider_url']['id'] = 'field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['table'] = 'field_data_field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['field'] = 'field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slider_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_url']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_url']['empty'] = 'No link!';
  $handler->display->display_options['fields']['field_banner_slider_url']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_url']['click_sort_column'] = 'url';
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'banner_slider:block_slidesorter';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '1';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'banner_slider' => 'banner_slider',
  );

  /* Display: Home page */
  $handler = $view->new_display('block', 'Home page', 'banner_slider_home');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Target path or URL */
  $handler->display->display_options['fields']['field_banner_slider_url']['id'] = 'field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['table'] = 'field_data_field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['field'] = 'field_banner_slider_url';
  $handler->display->display_options['fields']['field_banner_slider_url']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slider_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slider_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_url']['click_sort_column'] = 'url';
  /* Field: Content: Banner slider image */
  $handler->display->display_options['fields']['field_banner_slider_img']['id'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['table'] = 'field_data_field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['field'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slider_img']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slider_img']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slider_img']['alter']['path'] = '[field_banner_slider_url]';
  $handler->display->display_options['fields']['field_banner_slider_img']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_banner_slider_img']['settings'] = array(
    'image_style' => 'banner_slide',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_banner_slider_url]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '<div class="banner-slide-text">
<h2 class="banner-slide-title">[title]</h2>
<div class="banner-slide-caption">[body]</div>
<!--<div class="banner-slide-attrib">[field_banner_slide_credit]</div>-->
</div>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Image Source/Attribution */
  $handler->display->display_options['fields']['field_banner_slide_credit']['id'] = 'field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['table'] = 'field_data_field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['field'] = 'field_banner_slide_credit';
  $handler->display->display_options['fields']['field_banner_slide_credit']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slide_credit']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_banner_slide_credit']['type'] = 'text_plain';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_type'] = 'div';
  $handler->display->display_options['fields']['edit_node']['element_class'] = 'edit-link';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['text'] = 'Edit this slide';
  $handler->display->display_options['block_description'] = 'Banner slider (home)';
  $handler->display->display_options['block_caching'] = '8';

  /* Display: Admin page */
  $handler = $view->new_display('page', 'Admin page', 'page_1');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'draggableviews' => 'draggableviews',
    'field_banner_slider_img' => 'field_banner_slider_img',
    'title' => 'title',
    'body' => 'title',
    'field_banner_slide_credit' => 'title',
    'field_banner_slider_url' => 'title',
    'edit_node' => 'edit_node',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'draggableviews' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_banner_slider_img' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_banner_slide_credit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_banner_slider_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<div class="messages help"><h2>You can sort your slides by their drag handles below.</h2></div>';
  $handler->display->display_options['header']['area']['format'] = '2';
  $handler->display->display_options['path'] = 'admin/content/banners';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Banner Slides';
  $handler->display->display_options['menu']['description'] = 'Banner Slides';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 1;

  /* Display: Unpublished slides */
  $handler = $view->new_display('attachment', 'Unpublished slides', 'banner_slide_unpublished');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_banner_slide_credit' => 'field_banner_slide_credit',
    'body' => 'body',
    'field_banner_slider_img' => 'field_banner_slider_img',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_banner_slide_credit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_banner_slider_img' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Banner slider image */
  $handler->display->display_options['fields']['field_banner_slider_img']['id'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['table'] = 'field_data_field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['field'] = 'field_banner_slider_img';
  $handler->display->display_options['fields']['field_banner_slider_img']['label'] = '';
  $handler->display->display_options['fields']['field_banner_slider_img']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_banner_slider_img']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_banner_slider_img']['settings'] = array(
    'image_style' => 'banner_slide_full',
    'image_link' => 'content',
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '0';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'banner_slider' => 'banner_slider',
  );
  $handler->display->display_options['displays'] = array(
    'page_1' => 'page_1',
    'default' => 0,
    'banner_slides_full' => 0,
    'banner_slides_unpublished' => 0,
    'banner_slides2' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';
  $export['banner_slider'] = $view;

  return $export;
}
